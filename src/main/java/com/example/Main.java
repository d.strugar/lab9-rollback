package com.example;

import java.sql.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "labs";
    static final String PASS = "lab9pass";

    public static void main(String[] args) {
        add_salaries(new LinkedList<Integer>(List.of(1, 2, 3)),
            new LinkedList<Integer>(List.of(106, 107, 108)),
            new LinkedList<Integer>(List.of(12000, 24000, 36000))
        );
    }

    static void add_salaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids, LinkedList<Integer> amount) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);


            System.out.println("Creating statement...");
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            Statement finalStmt = stmt;
            IntStream.range(0, ids.size()).forEach(i -> {
                System.out.println("Inserting one row...." + i);
                String SQL = "INSERT INTO Salaries " +
                        "VALUES (" + ids.get(i) + "," +
                        employee_ids.get(i) + "," +
                        amount.get(i) + ");";

                try {
                    finalStmt.executeUpdate(SQL);
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            });

            System.out.println("Commiting data here....");
            conn.commit();

            ResultSet rs = stmt.executeQuery("SELECT * FROM Salaries");
            System.out.println("List result set for reference....");
            printSalaries(rs);

            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Rolling back data here....");
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    static void printSalaries(ResultSet resultSet) throws SQLException {
        resultSet.beforeFirst();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            int employeeId = resultSet.getInt("employee_id");
            int amount = resultSet.getInt("amount");

            System.out.println("id: " + id + "; employee id: " + employeeId + "; amount: " + amount);
        }
    }
}
